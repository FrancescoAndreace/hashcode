## ALPACA PROJECT - PASTEUR INSTITUTE - GOOGLE HASHCODE PIZZA SLICING
by Francesco Andreace

To compute the solution download 'utils.py' and 'Francesco_Andreace_Solution.ipynb'. The solution to the input is in 'input_7_solved.txt'.
Utils.py contains all the static methods used to compute the solution. Every method is explained.
Francesco_Andreace_Solution.ipynb is a Jupyter Notebook that contains all the explanations of every choice made.
Here you can find the summary:
To compute the solution two arrays are created from the input file: pizza and pizzaMask. The first is the representation of the pizza with ones where the ham is and zeros otherwise; 
the second is used to keep track of which parts of the pizza have been cut: ones are where the pizza has been cut and zeros otherwise.  
To compute the solution all the possible shapes of a pizza are computed and then sorted. The order chosen is: First the slices with bigger area values and then if equals prioritize slices that are more compact. This choice is made to optimize the way the pizza is cut and is related to the way the cuts are made.
To cut the pizza, the pizza matrix is scanned from left to right and from top to bottom (for every row check all columns - western way of writing). If in a particular point I can place
a slice (place a slice: the point is row1 and col1 - the slice will be cut to the right and to the bottom), I choose the best one (simply scanning down the sorted slices and choosing the
first, that is valid (does not overlap with previous slices or goes out of the pizza) and that is ultimate (has the minimum required number of ham).
Scanning this way the matrix I try to have cut in the best way at each step (greedy) the pizza and from the point in which I am, I have the top and left part of the matrix already cut.

Optimality is not guaranteed. The order chosen is good for this example and is not guaranteed to be the best in every case.