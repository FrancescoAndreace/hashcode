#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 14 2021

@author: francescoandreace
"""

#LIBRARIES

import numpy as np 


### METHODS ###

def get_pizza(filepath):
    """
    Takes as input the path of the "pizza" file and returns all the parameters useful to solve the problem

    Args:
        filepath: path to file containing the pizza

    Returns:
        pizzaMask: matrix with the same shape of the pizza full of zeros. Ones will be put where a slice is cut.
        pizza:the pizza matrix made of a numpy ndarray whith 0 == T & 1 == H ; 
        R: # of rows; 
        C: # of columns; 
        H: min # of Hs to consider a slice valid;
        S: max # of squares to consider a slice valid;
    """

    with open(filepath,'r') as f:
        pizzaFile = f.read().splitlines()
        
    R,C,H,S = list(map(int,pizzaFile[0].split(' ')))

    pizza = np.zeros((R, C), np.int8)

    for i in range(1,R+1):
        for j in range(C):
            if pizzaFile[i][j] == 'H':
                pizza[i-1,j] = 1
    
    pizzaMask = np.zeros((R, C), np.int8)

    return pizzaMask,pizza,R,C,H,S


def get_shapes(minH,maxS):
    """
    Takes as input the min # of ham squares and the max # of squares in a valid ultimate slice

    Args:
        minH: min ham squares ;
        maxS: max # of squares ;

    Returns:
        slices: a list containing all the possible slices given the 2 input parameters
    """
    shapes = []
    #creating valid shapes...

    for i in range(1,maxS+1):
        for j in range(1,maxS+1):
            area = i*j
            if  minH <= area <= maxS:
                shapes.append((i,j))

    #sorting tuples FIRST by AREA and if equals by COMPACTNESS
    final_shapes =  sorted(shapes, key=lambda tupl: (-tupl[0]*tupl[1], tupl[0]+tupl[1]))
    del shapes
    return final_shapes


def isValid(anchor,ultSlice,pizzaMask,R,C):
    """
    Takes as input the current anchor, the potential ultimate slice, the mask of cut pizza, max rows and columns
    and returns if the candidate slice is valid (i.e. does not overlap with other slices and does not go out of the pizza)

    Args:
        anchor: is the current place in the matrix where I try to start placing the slice;
        ultSlice: is the candidate ultimate slice (tuple);
        pizzaMask: the matrix that is used as mask on the pizza to check if a portion is cut;
        R: max rows;
        C: max columns;
        

    Returns:
        True: if valid; False: if not.
    """
    #check if it goes out of bounds
    if anchor[0]+ultSlice[0] > R or anchor[1]+ultSlice[1] > C:
        return False

    #check if overlaps with a square already cut
    for j in range(anchor[1],anchor[1]+ultSlice[1]):
        for i in range(anchor[0],anchor[0]+ultSlice[0]):
            if pizzaMask[i,j] == 1:
                return False
    return True


def isUltimate(anchor,ultSlice,pizza,n_ham):
    """
    Takes as input the current anchor, the potential ultimate slice, the pizza and number of required ham squares
    and returns if the candidate slice is ULTIMATE (i.e. does containt at least n_ham squares)

    Args:
        anchor: is the current place in the matrix where I try to start placing the slice;
        ultSlice: is the candidate ultimate slice (tuple);
        pizza: the pizza to check where the ham is;
        n_ham: min ham squares required;
        

    Returns:
        True: if utlimate; False: if not.
    """
    #check if there are at least 3 pieces of ham
    ham = 0
    
    for j in range(anchor[1],anchor[1]+ultSlice[1]):
        for i in range(anchor[0],anchor[0]+ultSlice[0]):
            ham += pizza[i,j]
            if ham == n_ham:
                return True
    return False


def cut_slice(anchor,ultSlice,pizzaMask):
    """
    Takes as input the current anchor, the ultimate slice, the pizzamask
    and places 1s on the mask where the slice has been cut

    Args:
        anchor: is the current place in the matrix where I try to start placing the slice;
        ultSlice: is the ultimate slice (tuple);
        pizzaMask: the matrix that is used as mask on the pizza to check if a portion is cut;
        n_ham: min ham squares required;
        

    Returns:
        True: if utlimate; False: if not.
    """
    #cut the slice in the mask (put elements = 1)
    for j in range(anchor[1],anchor[1]+ultSlice[1]):
        for i in range(anchor[0],anchor[0]+ultSlice[0]):
            pizzaMask[i,j] = 1


def place_slice(anchor,slices,pizza,pizzaMask,R,C,n_ham):
    """
    Takes as input the current anchor, the possible slices,the pizza, the pizzamask, the max # of rows, the max # of columns and 
    the min ham required for every slice and cuts the BEST slice possible at that position. Optimality is not guaranteed.

    Args:
        anchor: is the current place in the matrix where I try to start placing the slice;
        ultSlice: is the ultimate slice (tuple);
        pizza: the pizza to check where the ham is;
        pizzaMask: the matrix that is used as mask on the pizza to check if a portion is cut;
        R: max rows;
        C: max columns;
        n_ham: min ham squares required;
        

    Returns:
        Object: if no cut is made returns None, else it returns the anchor and the slice used.
    """
    for i in range(len(slices)):
        if isValid(anchor,slices[i],pizzaMask,R,C) == True and isUltimate(anchor,slices[i],pizza,n_ham) == True:
            cut_slice(anchor,slices[i],pizzaMask)
            #print('anchor',anchor)
            #print('slice',slices[i])
            return(anchor,slices[i])
    return None

